import { Observable, EMPTY } from 'rxjs';
import { Route } from '@angular/router';

export class OptionalPreloadstrategy {

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return route.data && route.data.preload ? load() : EMPTY;
  }

}
