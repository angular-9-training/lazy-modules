import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of , EMPTY} from 'rxjs';
import { Injectable } from '@angular/core';

import { OnDemandPreloadOptions, OnDemandPreloadService } from '../services/on-demand-preload.service';
import { mergeMap } from 'rxjs/operators';


/**
 * Estrategia de precarga bajo demanda
 * esta estrategia precarga los modulos siempre que se solicite
 * y la ruta solicitada tenga las caracteristicas adecuadas
 *  - preload a true
 *  - sea una ruta valida
 * Desde la vista de cualquier componente o su controlador, podriamos
 * precargar una ruta dinámicamente gracias al servicio creado
 * 
 * Para este ejemplo, el componente NAV contendra los botones
 * que precargarían módulos sin la necesidad de navegar a ellos
 * 
 */

//Para poder user DI en los componentes
@Injectable({
  providedIn: 'root',
  deps : [OnDemandPreloadService]
})  
export class OnDemandPreloadStrategy implements PreloadingStrategy {

  //Declaramos una variable que apunte al observable del servicio
  private preloadOnDemand$: Observable<OnDemandPreloadOptions>;

  constructor(private onDemandPreloadService: OnDemandPreloadService) {
    this.preloadOnDemand$ = this.onDemandPreloadService.state;
  }

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return this.preloadOnDemand$.pipe(
      mergeMap(preloadOptions => {
        //Deberia precargar la ruta o no
        const shouldPreload = this._preloadCheck(route, preloadOptions);
        console.log(`${shouldPreload ? '' : 'NO'} precarga la ruta ${route.path}`);
        
        return shouldPreload ? load() : EMPTY;
      })
    );
  }

  //Método para saber si debemos precargar la ruta o no
  private _preloadCheck(route: Route, preloadOptions: OnDemandPreloadOptions) {
    return (
      route.data
      && route.data.preload
      && [route.path, '*'].includes(preloadOptions.routePath)
      && preloadOptions.preload
    );

  }

}
