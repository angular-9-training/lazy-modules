import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export class OnDemandPreloadOptions {
  constructor(public routePath: string, public preload: boolean= true){}
}

@Injectable({
  providedIn: 'root'
})
export class OnDemandPreloadService {

  private subject = new Subject<OnDemandPreloadOptions>();
  state = this.subject.asObservable();

  constructor() { }

  startPreload(routePath: string) {
    const carga = new OnDemandPreloadOptions(routePath, true);
    this.subject.next(carga);
  }
}
