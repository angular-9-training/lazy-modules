import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { OnDemandPreloadStrategy } from './preloading-strategies/on-demand-preload-strategy';
import { NetworkAwarePreloadStrategy } from './preloading-strategies/network-aware-strategy';


const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(p => p.HomeModule),
    data : { preload: true }
  },
  {
    path: 'contacts',
    loadChildren: () => import('./modules/contacts/contacts.module').then(p => p.ContactsModule),
    data : { preload: true }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    //{ preloadingStrategy: PreloadAllModules }
    //{ preloadingStrategy: OnDemandPreloadStrategy }
    { preloadingStrategy: NetworkAwarePreloadStrategy}
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
