import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './components/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { OptionalPreloadstrategy } from './preloading-strategies/optional-preloadstrategy';
import { OnDemandPreloadStrategy } from './preloading-strategies/on-demand-preload-strategy';
import { NetworkAwarePreloadStrategy } from './preloading-strategies/network-aware-strategy';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [
    OptionalPreloadstrategy,
    OnDemandPreloadStrategy,
    NetworkAwarePreloadStrategy
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
