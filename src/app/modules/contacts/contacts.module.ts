import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { ContactDetailsComponent } from './pages/contact-details/contact-details.component';


@NgModule({
  declarations: [ContactsComponent, ContactDetailsComponent],
  imports: [
    CommonModule,
    ContactsRoutingModule
  ]
})
export class ContactsModule { }
